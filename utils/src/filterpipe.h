#ifndef __PIPE_H__
#define __PIPE_H__

typedef struct __pipe
{
    struct __pipe *self;
    void *         inb;
    void *         outb;
    size_t         inmax;
    size_t         outmax;

    void (*get_data) (void *in, size_t bytes, struct __pipe *self);
    void (*send_data) (void *out, size_t bytes, struct __pipe *self);
} stPIPE;

typedef struct __filter
{
    char    name[255];
    stPIPE *inp;
    stPIPE *outp;
    int (*execute) (struct __filter *self, void *inbuffer, void *outbuffer);
} stFILTER;

stPIPE *  create_pipe (void * inbuffer,
                       size_t inmax,
                       void * outbuffer,
                       size_t outmax,
                       void (*get_data) (void *in, size_t bytes, stPIPE *self),
                       void (*send_data) (void *out, size_t bytes, stPIPE *self));
stFILTER *create_filter (const char *name,
                         int (*func) (stFILTER *self,
                                      void *    inbuffer,
                                      void *    outbuffer),
                         stPIPE *in,
                         stPIPE *out);
int       register_filter (stFILTER *filter);
int       initialize_filterpipe (size_t num);
void      run_loop_filters (void);

#endif