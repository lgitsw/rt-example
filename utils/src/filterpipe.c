#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "filterpipe.h"

stFILTER **filter_chain;
size_t     filter_num   = 0;
int        filter_index = 0;

int initialize_filterpipe (size_t num)
{
    filter_chain = calloc (num, sizeof (stFILTER *));
    if (filter_chain == NULL)
        return -1;
    memset ((void *)filter_chain, 0, sizeof (stFILTER *) * num);

    filter_num = num;
    return 0;
}

stPIPE *create_pipe (void * inbuffer,
                     size_t inmax,
                     void * outbuffer,
                     size_t outmax,
                     void (*get_data) (void *in, size_t bytes, stPIPE *self),
                     void (*send_data) (void *out, size_t bytes, stPIPE *self))
{
    stPIPE *pipe = NULL;
    pipe         = (stPIPE *)malloc (sizeof (stPIPE));
    if (pipe == NULL)
        return NULL;

    memset ((void *)pipe, 0, sizeof (stPIPE));

    pipe->inb       = inbuffer;
    pipe->outb      = outbuffer;
    pipe->inmax     = inmax;
    pipe->outmax    = outmax;
    pipe->get_data  = get_data;
    pipe->send_data = send_data;
    pipe->self      = pipe;

    return pipe;
}

stFILTER *create_filter (const char *name,
                         int (*func) (stFILTER *self,
                                      void *    inbuffer,
                                      void *    outbuffer),
                         stPIPE *in,
                         stPIPE *out)
{
    stFILTER *filterp = NULL;
    filterp           = (stFILTER *)malloc (sizeof (stFILTER));
    if (filterp == NULL)
        return NULL;

    memset ((void *)filterp, 0, sizeof (stFILTER));
    strcpy (filterp->name, name);

    filterp->inp     = in;
    filterp->outp    = out;
    filterp->execute = func;

    return filterp;
}

int register_filter (stFILTER *filter)
{
    if ((filter_index + 1) >= filter_num)
        return -1;

    filter_chain[filter_index] = filter;

    filter_index++;

    return 0;
}

void run_loop_filters (void)
{
    int       index = 0;
    stFILTER *fp = NULL, *prevfp = NULL;

    if (filter_num > 0)
    {
        do
        {
            if (index >= filter_index)
            {
                index  = 0;
                prevfp = NULL;
            }
            fp = filter_chain[index];

            if (fp->inp->get_data)
                fp->inp->get_data (fp->inp->inb, fp->inp->inmax, fp->inp);
            else
            {
                if (prevfp != NULL)
                    memcpy ((void *)fp->inp->inb,
                            (const void *)prevfp->outp->outb,
                            fp->inp->inmax);
            }

            fp->execute (filter_chain[index], fp->inp->inb, fp->outp->outb);

            if (fp->outp->send_data)
                fp->outp->send_data (
                    fp->outp->outb, fp->outp->outmax, fp->outp);

            prevfp = fp;

            index++;
        } while (1);
    }
}
