#include <unistd.h>
#include <stdio.h>
#include "filterpipe.h"

static int testdata = 0;

void start_read (void *in, size_t bytes, stPIPE *self)
{
    testdata++;
    *(int *)in = testdata;
    printf ("start read %d\n", *(int *)in);
}

void end_write (void *out, size_t bytes, stPIPE *self)
{
    int data = *(int *)out;
    printf ("end write %d\n", data);
}

int filter_preprocessing (stFILTER *self, void *inbuffer, void *outbuffer)
{
    int data;

    data = *((int *)inbuffer);

    // Processing data
    data += 1;

    printf ("%s data processed %d\n", self->name, data);

    *((int *)outbuffer) = data;

    return 0;
}

int filter_postprocessing (stFILTER *self, void *inbuffer, void *outbuffer)
{
    int data;

    data = *((int *)inbuffer);

    // Processing data
    data += 2;

    printf ("%s data processed %d\n", self->name, data);

    *((int *)outbuffer) = data;

    return 0;
}

int main (void)
{
    initialize_filterpipe (10);

    int inb = 0, outb = 0;

    printf ("Creating pipes...\n");
    stPIPE *start_pipe = create_pipe (
        &inb, sizeof (int), &outb, sizeof (int), start_read, NULL);
    stPIPE *inout_pipe
        = create_pipe (&inb, sizeof (int), &outb, sizeof (int), NULL, NULL);
    stPIPE *end_pipe = create_pipe (
        &inb, sizeof (int), &outb, sizeof (int), NULL, end_write);

    printf ("Creating filters...\n");
    stFILTER *filterA = create_filter (
        "Filter Pre-processing", filter_preprocessing, start_pipe, inout_pipe);
    stFILTER *filterB = create_filter (
        "Filter B", filter_postprocessing, inout_pipe, inout_pipe);
    stFILTER *filterC = create_filter (
        "Filter C", filter_postprocessing, inout_pipe, inout_pipe);
    stFILTER *filterD = create_filter (
        "Filter Post-processing", filter_postprocessing, inout_pipe, end_pipe);

    printf ("Registering filters...\n");
    register_filter (filterA);
    register_filter (filterB);
    register_filter (filterC);
    register_filter (filterD);

    printf ("Run infinite loop\n");
    run_loop_filters ();

    return 0;
}
