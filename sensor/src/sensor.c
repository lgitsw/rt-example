#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>

#include "sensor.h"

void report_and_exit (const char *msg)
{
    perror (msg);
    exit (-1);
}

int main ()
{
    int fd = shm_open (BackingFile,      /* name from smem.h */
                       O_RDWR | O_CREAT, /* read/write, create if needed */
                       AccessPerms);     /* access permissions (0644) */
    if (fd < 0)
        report_and_exit ("Can't open shared mem segment for hall sensing...");

    ftruncate (fd, sizeof (stSensorData)); /* get the bytes */

    caddr_t memptr = mmap (NULL, /* let system pick where to put segment */
                           sizeof (stSensorData),  /* how many bytes */
                           PROT_READ | PROT_WRITE, /* access protections */
                           MAP_SHARED, /* mapping visible to other processes */
                           fd,         /* file descriptor */
                           0);         /* offset: start at 1st byte */
    if ((caddr_t)-1 == memptr)
        report_and_exit ("Can't get segment...");

    fprintf (stderr,
             "shared mem address: %p [0..%d]\n",
             memptr,
             sizeof (stSensorData) - 1);
    fprintf (stderr, "backing file:       /dev/shm%s\n", BackingFile);

    stSensorData readed;
    readed.gyro = 0;
    readed.hall = 1024;
    int index   = 0;

    do
    {
        // usleep(1);
        memcpy (memptr, &readed, sizeof (readed));
        readed.gyro++;
        readed.hall++;

        if (index++ == 100)
        {
            readed.gyro = 0;
            readed.hall = 1024;
            index       = 0;
        }
    } while (true);

    /* clean up */
    munmap (memptr, sizeof (stSensorData)); /* unmap the storage */
    close (fd);
    shm_unlink (BackingFile);
    return 0;
}