#ifndef __SENSOR_APP_H__
#define __SENSOR_APP_H__

#include <sys/ipc.h>
#include <sys/shm.h>

#define BackingFile "sensor"
#define SemaphoreName "sensor_sem"

#define AccessPerms (0644 | IPC_CREAT)

typedef struct __sensor_data
{
    unsigned int hall;
    unsigned int gyro;
} stSensorData;

#endif