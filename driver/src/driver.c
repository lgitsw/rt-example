#include <fcntl.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdbool.h>

#include "filterpipe.h"
#include "sensor.h"

int     fd;
caddr_t memptr = NULL;
sem_t * semptr = NULL;

typedef struct __processedData
{
    unsigned int a;
    unsigned int b;
    unsigned int c;
} stProcessedData;

void report_and_exit (const char *msg)
{
    perror (msg);
    exit (-1);
}

void start_read (void *in, size_t bytes, stPIPE *self)
{
    if (memptr)
    {
        memcpy ((void *)in, memptr, bytes);
    }
    printf ("start read %d:%d\n",
            ((stSensorData *)in)->hall,
            ((stSensorData *)in)->gyro);
}

void end_write (void *out, size_t bytes, stPIPE *self)
{
    stProcessedData *data = (stProcessedData *)out;
    printf ("end write %d:%d:%d\n", data->a, data->b, data->c);
}

int filter_preprocessing (stFILTER *self, void *inbuffer, void *outbuffer)
{
    stSensorData *   data = (stSensorData *)inbuffer;
    stProcessedData *out  = (stProcessedData *)outbuffer;

    data = ((stSensorData *)inbuffer);

    // Processing data
    out->a = data->hall + 1;
    out->b = data->gyro + 1;
    out->c = data->hall + data->gyro;

    printf ("%s data processed %d:%d:%d\n", self->name, out->a, out->b, out->c);

    memcpy ((void *)outbuffer, (void *)out, sizeof (stProcessedData));

    return 0;
}

int filter_postprocessing (stFILTER *self, void *inbuffer, void *outbuffer)
{
    stProcessedData *data = (stProcessedData *)inbuffer;

    // Processing data
    data->a += 1;
    data->b += 1;
    data->c += 1;

    printf (
        "%s data processed %d:%d:%d\n", self->name, data->a, data->b, data->c);

    memcpy ((void *)outbuffer, (void *)data, sizeof (stProcessedData));

    return 0;
}

int main ()
{
    do
    {
        fd     = shm_open (BackingFile, O_RDWR, AccessPerms);
        memptr = mmap (NULL,
                       sizeof (stSensorData),
                       PROT_READ | PROT_WRITE,
                       MAP_SHARED,
                       fd,
                       0);
        if ((caddr_t)-1 == memptr)
        {
            printf ("Can't access shared memory...\n");
            sleep (5);
        }
        else
        {
            break;
        }
    } while (true);

    initialize_filterpipe (10);

    stSensorData    sensorIn;
    stProcessedData dataIn, dataOut;

    printf ("Creating pipes...\n");
    stPIPE *start_pipe = create_pipe (&sensorIn,
                                      sizeof (sensorIn),
                                      &dataOut,
                                      sizeof (dataOut),
                                      start_read,
                                      NULL);
    stPIPE *inout_pipe = create_pipe (
        &dataIn, sizeof (dataIn), &dataOut, sizeof (dataOut), NULL, NULL);
    stPIPE *end_pipe = create_pipe (
        &dataIn, sizeof (dataIn), &dataOut, sizeof (dataOut), NULL, end_write);

    printf ("Creating filters...\n");
    stFILTER *filterA = create_filter (
        "Filter Pre-processing", filter_preprocessing, start_pipe, inout_pipe);
    stFILTER *filterB = create_filter (
        "Filter B", filter_postprocessing, inout_pipe, inout_pipe);
    stFILTER *filterC = create_filter (
        "Filter C", filter_postprocessing, inout_pipe, inout_pipe);
    stFILTER *filterD = create_filter (
        "Filter Post-processing", filter_postprocessing, inout_pipe, end_pipe);

    printf ("Registering filters...\n");
    register_filter (filterA);
    register_filter (filterB);
    register_filter (filterC);
    register_filter (filterD);

    printf ("Run infinite loop\n");
    run_loop_filters ();

    /* cleanup */
    munmap (memptr, sizeof (stSensorData));
    close (fd);
    unlink (BackingFile);
    return 0;
}
